/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.midterm2;

/**
 *
 * @author Satit Wapeetao
 */
public class Team {
    protected String name;
    protected String surname;
    protected String age;
    protected String Height;
    protected String Weight;
    protected String position;
    public Team(String name,String surname,String age,String Height,String Weight,String position){
        this.name=name;
        this.surname=surname;
        this.age=age;
        this.Height=Height;
        this.Weight=Weight;
        this.position=position;
    }
    public String toString(){
        return this.name+this.surname+this.age+this.Height+this.Weight+this.position;
}
    public void Color(){
        System.out.print("Hair Color is ");
    }
    public void Color(String color){
        System.out.println(color);
    }
}
