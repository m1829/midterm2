/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.midterm2;

/**
 *
 * @author Satit Wapeetao
 */
public class TestProject {
    public static void main(String[] args) {
        Character1 character1 = new Character1("Seijuro","Akashi","16","158","64","Point Guard");
        System.out.println(character1);
        character1.Color();
        character1.Color("Red");
        Character2 character2 = new Character2("Mibuchi","Reo","17","188","74","Shooting Guard");
        System.out.println(character2);
        character2.Color();
        character2.Color("Black");
        Character3 character3 = new Character3("Hayama","Kotaro","17","180","68","Small Forward");
        System.out.println(character3);
        character3.Color();
        character3.Color("Yello");
        Character4 character4 = new Character4("Nebuya","Eikichi","17","190","94","Center");
        System.out.println(character4);
        character4.Color();
        character4.Color("Black");
        Character5 character5 = new Character5("Mayuzumi","Chihiro","18","182","69","Power Forward");
        System.out.println(character5);
        character5.Color();
        character5.Color("Grey");
}
}
