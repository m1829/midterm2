/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.midterm2;

/**
 *
 * @author Satit Wapeetao
 */
public class Character3 extends Team{
    
    public Character3(String name, String surname, String age, String Height, String Weight, String position) {
        super(name, surname, age, Height, Weight, position);
    }
    @Override
    public String toString(){
        return "Name "+this.name+" Surname "+this.surname+"\n"+"Age "+this.age+
                "\n"+"Height "+this.Height+" Weight "+this.Weight+
                "\n"+"Position "+this.position;
}
    @Override
    public void Color(String color){
        System.out.println(color+"\n");
    }
}
